<?php
date_default_timezone_set('America/Sao_Paulo');
if(file_exists(__DIR__.'/../development.env')){
	require(__DIR__.'/development.php');
}else{
	require(__DIR__.'/production.php');
}
require(DIRPATH.'/class/class.sistema.php');

$sistema = new Sistema();

$title = TITLE;
$url = URL;
$description = '';
$keywords = '';
$class = '';
$imgshare = '';