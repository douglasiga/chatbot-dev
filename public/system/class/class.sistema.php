<?php
require_once('class.db.php');
require_once('class.defaults.php');
class Sistema extends Defaults{

	private $database;

	public function __construct() {

		session_start(SS_NAME);

		$this->database = new Database();

	}

	public function get_flow($cod){
		$arr = array();
		$sql = $this->getSimple('select id, type_id, content_text, loading_time, loading_writting from tpc_questions where flow_id = :flowid order by display_order', array('flowid' => $cod));
		foreach($sql as $key => $qry){
			$qry['answer'] = '';
			if($qry['type_id'] != '1'){
				$qrya = $this->getSimple('select id, field_name, field_placeholder from tpc_answers where question_id = :question_id limit 1', array('question_id' => $qry['id']), false);
				if($qrya){
					$qry['answer']['name'] = $qrya['field_name'];
					$qry['answer']['placeholder'] = $qrya['field_placeholder'];

					if($qry['type_id'] == '3' || $qry['type_id'] == '5'){
						$sqloptions = $this->getSimple('select content_value from tpc_answers_options where answer_id = :answer_id order by display_order', array('answer_id' => $qrya['id']));
						if(sizeof($sqloptions) > 0){
							foreach($sqloptions as $keyoptions => $qryoptions){
								$qry['answer']['options'][$keyoptions] = $qryoptions['content_value'];
							}
						}
					}elseif($qry['type_id'] == '4'){
						$qry['answer']['options'] = array('Sim','Não');
					}
				}
			}

			$arr[$key] = $qry;
		}
		return $arr;
	}

}