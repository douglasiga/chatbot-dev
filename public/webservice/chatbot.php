<?php 
header('Content-Type: application/json');
require(__DIR__.'/../system/config.php');

$arr = array('success' => false);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(isset($_POST['cod']) and !empty($_POST['cod'])){
		$arr['chatbot'] = $sistema->get_flow($_POST['cod']);
		$arr['success'] = true;
	}
}

echo json_encode($arr);