<meta charset="utf-8">
<title><?=$title?></title>

<meta name="DC.title" content="<?=$title?>"/>
<meta name="description" content="<?=$description?>"/>
<link rel="canonical" href="<?=$url?>" />

<meta property="og:locale" content="pt_BR" />
<meta property="og:site_name" content="<?=TITLE?>" />
<meta property="og:title" content="<?=$title?>" />
<meta property="og:type" content="website"/>
<meta property="og:description" content="<?=$description?>" />
<meta property="og:image" content="<?=$imgshare?>"/>
<meta property="og:url" content="<?=$url?>"/>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<meta http-equiv="cache-control" content="public" />

<meta name="GOOGLEBOT" content="INDEX, FOLLOW" />
<meta name="msnbot" content="NOODP" />
<meta name="resource-type" content="document"/>
<meta name="content-language" content="pt-br"/>
<meta name="revisit" content="1 day"/>
<meta name="robots" content="follow, all">
<meta name="rating" content="General">
<meta name="author" content="<?=TITLE?>">

<link rel="stylesheet" href="<?=URLAPP?>css/style.min.css?v=<?php echo date('YmdHis'); ?>" media="screen" />

<link rel="shortcut icon" href="<?=URLAPP?>images/favicon.ico">
<link rel="apple-touch-icon" href="<?=URLAPP?>images/favicon-57x57.ico">
<link rel="apple-touch-icon" sizes="72x72" href="<?=URLAPP?>images/favicon-72x72.ico">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script async src="<?=URLAPP?>js/modernizr.lasted.min.js"></script>