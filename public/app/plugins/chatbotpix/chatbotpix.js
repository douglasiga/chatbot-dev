(function ($, window) {

  $.fn.chatbotpix = function(options) {

    var defaults = {
      'flow': 1,
      'stage': 0
    };

    var settings = $.extend({}, defaults, options);

    var current = {
      'chatbot': '',
      'stage': '',
      'inputs': []
    };

    var getBaseURL = function() {
      var url = document.URL;
      return url.substr(0, url.lastIndexOf('/'));
    }

    var flow = function(code){
      var arr = {
        'cod': code
      };
      $.ajax({
        url: getBaseURL() + "/webservice/chatbot.php",
        type: "POST",
        data: arr,
        dataType: 'json',
        success: function(data){
          if(data.success){
            chat(data.chatbot, defaults.stage);
          }else{
            return false;
          }
        },
        error: function(xhr, status, error) {
          console.log(xhr);
          console.log(status);
          console.log(error);
        }
      });
    };

    var checkVars = function(key){
      for(var i = current.stage;i<current.chatbot.length;i++){
        current.chatbot[i].content_text = current.chatbot[i].content_text.replace('%'+key+'%', current.inputs[key]);
      }
    }

    var message = function(msg, frm, loaded){
      var htm = '',
          dot = '<div class="pix-loading_dots"><span class="dot-1"></span><span class="dot-2"></span><span class="dot-3"></span></div>',
          msg = '<p>'+msg+'</p>',
          cla = '';

      if(frm == 'them'){
        var interact = dot;
        var cla = 'pix-loaded';
      }else{
        var interact = msg;
      }

      htm = '<div class="pix-message pix-from-' + frm + ' '+cla+'">' +
              '<div class="pix-message_row">' +
                '<div class="pix-message_photo table-cell">' +
                  '<figure></figure>' +
                '</div>' +
                '<div class="pix-message_text table-cell">' +
                  '<div>' + interact + '</div>' +
                '</div>' +
              '</div>' +
              '<div class="pix-message_footer">' +
                '<p><span class="pix-message_name">Trespix</span> <span class="pix-time" data-timestamp="1502724080326">3 minutos atrás</span></p>' +
              '</div>' +
            '</div>';
      $('#pix-chat_inner section').append(htm);
      $('#pix-chat_content').scrollTop($('#pix-chat_inner').height());

      if(frm == 'them'){
        setTimeout(function(){
          $('.pix-loaded .pix-message_text > div').html(msg);
          $('.pix-message').removeClass('pix-loaded');
          return true;
        }, loaded*1000);
      }else{
        return true;
      }

    };

    var chat = function(chatbot, stage){
      var leng = chatbot.length;

      current.chatbot = chatbot;
      current.stage = stage;

      if(stage<leng){

        message(chatbot[stage].content_text, 'them', chatbot[stage].loading_writting);
        setTimeout(function(){
          if(chatbot[stage].type_id == 1){
            chat(chatbot, stage+1);
          }else{
            view_input(chatbot[stage].type_id, chatbot[stage].answer, stage+1, chatbot);
          }
        }, (Number(chatbot[stage].loading_time)+Number(chatbot[stage].loading_writting))*1000);

      }

    };

    var view_input = function(type, answer){
      switch(type){
        case '2':
          create_text(answer, 'text');
          break;

        case '3':
          create_select(answer);
          break;

        case '4':
          create_select(answer);
          break;

        case '6':
          create_text(answer, 'email');
          break;
      }
    };

    var create_text = function(answer, type){
      var htm = '<div class="pix-input_content table-cell">' +
          '<input type="type" id="input-'+answer.name+'" name="'+answer.name+'" placeholder="'+answer.placeholder+'">' +
        '</div>' +
        '<div class="pix-send table-cell">' +
          '<button type="button" class="btn">Enviar</button>' +
        '</div>';

      $('#pix-chat_footer .pix-input_group').html(htm);
      $('#pix-chat_footer').addClass('active');

      $('#pix-chat_footer .btn').on('click', function(){
        var valor = $('#pix-chat_footer #input-'+answer.name).val();
        var vname = $('#pix-chat_footer #input-'+answer.name).attr('name');

        click_button(valor, vname);
      });
    }

    var create_select = function(answer){
      var opt = '<option value="">'+answer.placeholder+'</option>';
      for(var key in answer.options){
        opt += '<option value="'+answer.options[key]+'">'+answer.options[key]+'</option>';
      }
      var htm = '<div class="pix-input_content table-cell">' +
          '<select id="input-'+answer.name+'" name="'+answer.name+'">'+opt+'</select>' +
        '</div>' +
        '<div class="pix-send table-cell">' +
          '<button type="button" class="btn">Enviar</button>' +
        '</div>';

      $('#pix-chat_footer .pix-input_group').html(htm);
      $('#pix-chat_footer').addClass('active');

      $('#pix-chat_footer .btn').on('click', function(){
        var valor = $('#pix-chat_footer #input-'+answer.name).val();
        var vname = $('#pix-chat_footer #input-'+answer.name).attr('name');

        click_button(valor, vname);
      });
    }

    var click_button = function(valor, vname){
      if(valor && vname){
        message(valor, 'me');

        current.inputs[vname] = valor;
        checkVars(vname);
        
        $('#pix-chat_footer').removeClass('active');

        setTimeout(function(){
          chat(current.chatbot, current.stage+1);
        }, current.chatbot[current.stage].loading_time*1000);
      }
    }

    flow(defaults.flow);

  };

}(jQuery, window));