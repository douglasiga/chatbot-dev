<?php

	require_once('system/config.php');
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$acao = $_POST['acao'];
	
		switch($acao){
				
			case 'form':
				$cad = $sistema->forms->save_form();
				if($cad===true){
					$sistema->aledir('Contato enviado com sucesso!', URL);
				}else{
					$sistema->aledir('Ocorreu um erro ao enviar seu contato. Tente novamente!', URL);
				}
				break;

			case 'newsletter':
				$cad = $sistema->forms->save_newsletter();
				if($cad===true){
					$sistema->aledir('Seu email foi cadastrador com sucesso!', URL);
				}else{
					$sistema->aledir('Ocorreu um erro ao adicionar seu email. Tente novamente!', URL);
				}
				break;

			case 'search':
				$_SESSION['s'] = $sistema->cleanuserinput($_POST['s']);
				header('Location: '.URL.'buscar');
				exit();
				break;
			
		}

	}else{
		$sistema->load_error();
	}