<?php
include(__DIR__.'/system/config.php');
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="pt-BR" class="ie6 ie67"><![endif]-->
<!--[if IE 7]><html lang="pt-BR" class="ie7 ie67"><![endif]-->
<!--[if IE 8]><html lang="pt-BR" class="ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js <?=$class?>" lang="pt-BR">
<!--<![endif]-->
<head>
<?php include(DIRPATHP.'/app/includes/structure/header.php'); ?>
</head>

<body>
	<div id="pix-container">
		<?php include(DIRPATHP.'/app/includes/structure/topo.php'); ?>
		<main id="pix-chat_main">
			<div id="pix-chat_content">
				<div id="pix-chat_inner">

					<section>

					</section>

				</div>
			</div>
		</main>
		<footer id="pix-chat_footer">
			<div class="pix-input_group">
			</div>
		</footer>
	</div>
	<?php include(DIRPATHP.'/app/includes/structure/footer.php'); ?>
	<?php include(DIRPATHP.'/app/includes/structure/script.php'); ?>
</body>
</html>