/**
 * Gulpfile
 * @author Douglas Iga <doug.ishibashi@gmail.com>
 */

var gulp = require("gulp"),
  sass = require("gulp-sass"),
  concat = require("gulp-concat"),
  minifyCSS = require("gulp-minify-css"),
  uglify = require("gulp-uglify"),
  path = require("path"),
  combineMq = require('gulp-combine-mq'),
  livereload = require('gulp-livereload'),
  spritesmith = require('gulp.spritesmith');

var SASSPATH, CSSPATH, JSPATH;

gulp.task('sass', function(){
  gulp.src(SASSPATH + '/style.scss')
    .pipe(sass({errLogToConsole: true}))
    .pipe(combineMq())
    .pipe(concat('style.css'))
    .pipe(gulp.dest(CSSPATH));

  gulp.src(SASSPATH + '/style.scss')
    .pipe(sass({errLogToConsole: true}))
    .pipe(combineMq())
    .pipe(concat('style.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(CSSPATH))
    .pipe(livereload());
});

gulp.task('js', function(){
  gulp.src(JSSPATH + "/*.js")
    .pipe(concat("script.min.js"))
    .pipe(uglify({"compress": false}))
    .pipe(gulp.dest(JSPATH));
});

// watchers
livereload.listen();
var cssWatcher = gulp.watch('**/*.scss', ['sass']);
cssWatcher.on("change", function(event) {
  SASSPATH = 'dev/scss';
  CSSPATH  = 'public/app/css';
});

var jsWatcher = gulp.watch('**/dev/js/*.js', ['js']);
jsWatcher.on("change", function(event) {
  JSSPATH = 'dev/js';
  JSPATH = 'public/app/js';
});

/* Images Sprites */
gulp.task('sprite', function () {
    var spriteData = gulp.src('public/app/images/sprites/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: 'sprite.png',
            cssName: 'sprite.scss'
        }));
    spriteData.img.pipe(gulp.dest('public/app/images'));
    spriteData.css.pipe(gulp.dest('dev/scss/helpers'));
});

gulp.watch(['public/app/images/sprites/**/*.png'], ['sprite']);

gulp.task('watch', function () {
    gulp.watch(['public/app/images/sprites/**/*.png'], ['sprite']);
});
/* END Images Sprites */

gulp.task("default");